import { Component, OnInit } from '@angular/core';
import { AdminService } from '../admin.service';

@Component({
  selector: 'app-add-service',
  templateUrl: './add-service.component.html',
  styleUrls: ['./add-service.component.css']
})
export class AddServiceComponent implements OnInit {

  serviceName = '';

  constructor(private service : AdminService) { }

  addService() {
    console.log(this.serviceName);
    this.service
    .addService(this.serviceName)
    .subscribe(response=>{
    })
    alert("Service added successfully");
  }

  ngOnInit() {
  }

}
