import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AdminService } from '../admin.service';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.css']
})
export class AdminDashboardComponent implements OnInit {
  
  professionals = [];

  constructor(private router : Router, private service : AdminService) {
    this.service = service;
    this.loadProfessionals();
  }

  customersList(){
    this.router.navigate(['/ulist']);
  }

  loadProfessionals() {
    this.service
      .getProfessionals()
      .subscribe(response => {
        const body = response.json();
        this.professionals = body;
        console.log(response);
      });
  }

  onDelete(id) {
    console.log(id);
    const result = confirm('Are you sure you want to remove this user?');
    if (result) {
      this.service
        .delete(id)
        .subscribe(response => {
          this.loadProfessionals();
        })
    }
  }

  verifyProfessional(id) {
    console.log(id);
    const result = confirm('Are you sure you want to verify?');
    if (result) {
      this.service
        .verifyProfessional(id)
        .subscribe(response => {
          this.loadProfessionals();
        })
    }
  }

  addService() {
    this.router.navigate(['/add-service']);
  }

  readFeedback() {
    this.router.navigate(['/feedback']);
  }

  allOrdersList() {
    this.router.navigate(['/all-orders'])
  }

  ngOnInit() {
  }

}
