import { Component, OnInit } from '@angular/core';
import { AdminService } from '../admin.service';

@Component({
  selector: 'app-all-orders',
  templateUrl: './all-orders.component.html',
  styleUrls: ['./all-orders.component.css']
})
export class AllOrdersComponent implements OnInit {

  orders = [];

  constructor(private service : AdminService) { 
    this.loadOrders();
  }

  loadOrders(){
    this.service
      .getOrdersList()
      .subscribe(response => {
        const body = response.json();
        this.orders = body;
        console.log(this.orders);
      });
  }

  ngOnInit() {
  }

}
