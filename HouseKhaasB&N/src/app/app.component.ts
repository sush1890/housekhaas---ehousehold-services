import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  isNavbarCollapsed=true;
  title: 'HouseKhaas';

  valid : boolean = sessionStorage['validUser'];
  invalid : boolean = sessionStorage['invalidUser'];

  constructor(private router : Router){
    this.invalid = sessionStorage['invalidUser'] = true;
    this.valid = sessionStorage['validUser'] = false;
    console.log(this.invalid+" "+this.valid);
  }

  isLoggedIn(){
    if (sessionStorage['login_status'] == '1') {
      this.invalid = false;
      this.valid = true;
    }
  }

  onLoginClick(){
    if (sessionStorage['login_status'] == '1') {
      alert('You are already logged in');
    } else {
      this.router.navigate(['/user-login']);
    }  
  }

  onLogout(){
    if (sessionStorage['login_status'] == '1') {
      const result = confirm('Are you sure you want to logout?');
      if (result) {
        sessionStorage['login_status'] = '0';
        sessionStorage['invalidUser'] = true;
        sessionStorage['validUser'] = false;
        sessionStorage['uId'] = null;
        sessionStorage['user'] = null;
        document.getElementById("loginbtn").setAttribute("Style", "display:block");
        document.getElementById("logoutbtn").setAttribute("Style", "display:none");
        this.router.navigate(['/']);
        console.log(this.invalid+" "+this.valid)
      }
    } else {
      alert('You Are Not Logged In');
    }
  }

  profile() {
    if(sessionStorage['role']=='user'){
      this.router.navigate(["/uprofile-page"]);
    } else if(sessionStorage['role']=='professional') {
      this.router.navigate(['/pprofile-page']);
    } else if(sessionStorage['role']=='admin') {
      this.router.navigate(['/dashboard']);
    }
  }

  OnInit(){
   
  }
}