import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { RouterModule } from '@angular/router';
import { HomepageComponent } from './homepage/homepage.component';
import { ContactComponent } from './contact/contact.component';
import { OurServicesComponent } from './our-services/our-services.component';
import { UserRegisterComponent } from './user-register/user-register.component';
import { UserLoginComponent } from "./user-login/user-login.component";
import { ProLoginComponent } from './pro-login/pro-login.component';
import { ProRegisterComponent } from './pro-register/pro-register.component';
import { OrderComponent } from './order/order.component';
import { AdminLoginComponent } from './admin-login/admin-login.component';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { UserService } from './user.service';
import { ProService } from './pro.service';
import { UserProfilePageComponent } from './user-profile-page/user-profile-page.component';
import { ProProfilePageComponent } from './pro-profile-page/pro-profile-page.component';
import { AdminService } from './admin.service';
import { UserlistComponent } from './userlist/userlist.component';
import { AddServiceComponent } from './add-service/add-service.component';
import { FeedbacksComponent } from './feedbacks/feedbacks.component';
import { UpdateUserComponent } from './update-user/update-user.component';
import { UpdateProComponent } from './update-pro/update-pro.component';
import { OrderPoolComponent } from './order-pool/order-pool.component';
import { AllOrdersComponent } from './all-orders/all-orders.component';

@NgModule({
  declarations: [
    AppComponent,
    AboutUsComponent,
    HomepageComponent,
    ContactComponent,
    OurServicesComponent,
    UserRegisterComponent,
    UserLoginComponent,
    ProLoginComponent,
    ProRegisterComponent,
    OrderComponent,
    AdminLoginComponent,
    AdminDashboardComponent,
    UserProfilePageComponent,
    ProProfilePageComponent,
    UserlistComponent,
    AddServiceComponent,
    FeedbacksComponent,
    UpdateUserComponent,
    UpdateProComponent,
    OrderPoolComponent,
    AllOrdersComponent
  ],
  imports: [
    BrowserModule,
    NgbModule.forRoot(),
    HttpModule,
    FormsModule,
    RouterModule.forRoot([
      {path:'', component:HomepageComponent},
      {path: 'our-services', component:OurServicesComponent},
      {path:'about-us', component:AboutUsComponent},
      {path:'contact', component:ContactComponent},
      {path:'user-register', component:UserRegisterComponent},
      {path:'user-login', component:UserLoginComponent},
      {path:'pro-login', component:ProLoginComponent},
      {path:'pro-register', component:ProRegisterComponent},
      {path:'place-order', component:OrderComponent, canActivate:[UserService]},
      {path:'admin-login', component:AdminLoginComponent},
      {path:'dashboard', component:AdminDashboardComponent},
      {path:'uprofile-page', component:UserProfilePageComponent, canActivate:[UserService]},
      {path:'pprofile-page', component:ProProfilePageComponent},
      {path:'ulist', component:UserlistComponent},
      {path:'add-service', component:AddServiceComponent},
      {path:'feedback', component:FeedbacksComponent},
      {path:'update-user', component:UpdateUserComponent},
      {path:'update-pro', component:UpdateProComponent},
      {path:'order-pool', component:OrderPoolComponent},
      {path:'all-orders', component:AllOrdersComponent}
    ])
  ],
  providers: [
    UserService,
    ProService,
    AdminService
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
