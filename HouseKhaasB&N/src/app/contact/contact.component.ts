import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  oid:String;
  name:String;
  email:String;
  message:String;

  constructor(private service : UserService) { }

  feedbackSubmit() {
    this.service.submitFeedback(this.oid,this.name,this.email,this.message)
    .subscribe(response=>{
      alert('Your feedback has been submitted successfully');
    })
  }

  ngOnInit() {
  }

}
