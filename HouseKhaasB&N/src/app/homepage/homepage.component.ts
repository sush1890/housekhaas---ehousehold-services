import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {

  service='';
  id:number;

  constructor(private router : Router, private activeRoute : ActivatedRoute) { 
    activeRoute.queryParams.subscribe(param=>{
      this.id = param['id'];
    })
  }

  serviceSelect(event:any){
    console.log(event.target.value);
    this.service = event.target.value;
  }

  onServiceSelect(){
    this.router.navigate(['./place-order'], {queryParams:{'service':this.service, 'id':this.id}});
  }

  ngOnInit() {
  }

}
