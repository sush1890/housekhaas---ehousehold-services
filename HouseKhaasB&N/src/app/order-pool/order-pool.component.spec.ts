import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderPoolComponent } from './order-pool.component';

describe('OrderPoolComponent', () => {
  let component: OrderPoolComponent;
  let fixture: ComponentFixture<OrderPoolComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderPoolComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderPoolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
