import { Component, OnInit } from '@angular/core';
import { OrderService } from '../order.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-order-pool',
  templateUrl: './order-pool.component.html',
  styleUrls: ['./order-pool.component.css']
})
export class OrderPoolComponent implements OnInit {

  skill;
  orders = [];
  fid;

  constructor(private service : OrderService, private activeRoute : ActivatedRoute,private router:Router) { 
    activeRoute.queryParams.subscribe(param=>{
      this.skill = param["skill"];
      this.fid =  sessionStorage["uId"];
    })
  }

  getNewOrders(){
    this.service.getNewOrders(this.skill)
    .subscribe(response=>{
      console.log(response);
      const data = response.json();
      console.log(data);
      this.orders = data;
      console.log(this.orders);
    })
  }

  acceptOrder(id){
    console.log(id);
    const temp = parseInt(this.fid);
    console.log(this.fid);
    this.service
    .acceptOrder(id,temp)
    .subscribe(response=>{
      console.log(response);
      alert("Order has been added successfully");
      //this.sendEmail(id);
      this.ngOnInit();
    })
  }

  ngOnInit() {
    this.getNewOrders();
  }

}
