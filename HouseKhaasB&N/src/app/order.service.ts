import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  
  constructor(private http : Http) { }

  get(zip : any)
  {
    return this.http.get('http://localhost:8080/spring_mvc_hibernate_template/zipCode/zip/' + zip);
  }

  post(expectDate,expectTime,mobileNo,address,issue,service){
    const cid = sessionStorage['uId'];
    console.log(cid);
    const body = {
      issue:issue,
      cid:cid,
      address:address,
      expectDate:expectDate,
      mobileNo:mobileNo,
      expectTime:expectTime,
      service:service,
    }
    console.log(body);
    const headers = new Headers({'Content-Type' : 'application/json'});
    const requestOptions = new RequestOptions({headers:headers})

    return this.http.post('http://localhost:8080/spring_mvc_hibernate_template/orders/place-order', body, requestOptions);
  }

  getDetails(id: any) {
    console.log(id);
    return this.http.get('http://localhost:8080/spring_mvc_hibernate_template/customer/getDetails/' + id);
  }

  getNewOrders(skill:String){
    console.log(skill);
    const body = {
      skill:skill
    }
    console.log(body);
    const headers = new Headers({'Content-Type' : 'application/json'});
    const requestOptions = new RequestOptions({headers:headers})
    return this.http.post('http://localhost:8080/spring_mvc_hibernate_template/orders/newOrders',body,requestOptions);
  }

  acceptOrder(oid, fid){
    console.log(oid);
    const body = {
      oid:oid,
      pid:fid
    }
    console.log(body);
    const headers = new Headers({'Content-Type' : 'application/json'});
    const requestOptions = new RequestOptions({headers:headers})
    return this.http.post('http://localhost:8080/spring_mvc_hibernate_template/orders/acceptOrder',body,requestOptions);
  }

  orderCompleted(orderId) {
    const body = {
      oid:orderId
    }
    console.log(body);
    const headers = new Headers({'Content-Type' : 'application/json'});
    const requestOptions = new RequestOptions({headers:headers})
    return this.http.post('http://localhost:8080/spring_mvc_hibernate_template/orders/orderComplete',body,requestOptions);  
  }
}
