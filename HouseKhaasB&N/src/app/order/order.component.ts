import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OrderService } from '../order.service';
import { UserService } from '../user.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {

  service:String;
  mobileNo:String;
  address:String;
  issue:String;
  expectedDate:String;
  expectedTime:String;
  id= sessionStorage['uId'];

  constructor(private activeRoute : ActivatedRoute, private oservice : OrderService, private router : Router) {
    activeRoute.queryParams.subscribe(param=>{
      this.service = param["service"];
      console.log(this.service);

      this.getCustomerDetails(this.id);
    })
  }

  onOrderPlace(){
    this.oservice
    .post(this.expectedDate,this.expectedTime,this.mobileNo,this.address,this.issue,this.service)
    .subscribe(response=>{
      console.log(response);
    })
    alert("Order placed Sucessfully...");
    this.router.navigate(['/uprofile-page']);
  }

  getCustomerDetails(userId){
    console.log("inside order ts");
    this.oservice.getDetails(userId)
    .subscribe(response=>{
      const data = response.json();
      this.address = data['address'];
      this.mobileNo = data['mobileNo'];
    })
  }

  ngOnInit() {
  }

}
