import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProService } from '../pro.service';

@Component({
  selector: 'app-pro-login',
  templateUrl: './pro-login.component.html',
  styleUrls: ['./pro-login.component.css']
})
export class ProLoginComponent implements OnInit {

  email='';
  password='';

  constructor(private router : Router, private service : ProService) { }

  ngOnInit() {
  }

  onProLoginClick(){
    if (this.email.length == 0) {
      alert('enter email');
    } else if (this.password.length == 0) {
      alert('enter password');
    } else {
      this.service
        .login(this.email, this.password)
        .subscribe(response => {
          const body = response.json();
          console.log(body);
          console.log(response);
          if (body != null) {
            sessionStorage['validUser'] = true;
            sessionStorage['invalidUser'] = false;
            sessionStorage['login_status'] = '1';
            sessionStorage['role'] = 'professional';
            console.log(response["_body"]);
            sessionStorage['uId'] = response["_body"];
            alert('welcome to the app');
            document.getElementById("loginbtn").setAttribute("Style", "display:none");
            document.getElementById("logoutbtn").setAttribute("Style", "display:block");
            console.log(sessionStorage['invalidUser']+" "+sessionStorage['validUser']+" "+sessionStorage['login_status']);
            console.log(sessionStorage['uId']);
            this.router.navigate(['/pprofile-page'], {queryParams:{'id':response["_body"]}});
          } else {
            alert('Invalid username and password');  
          }
        });
        console.log(sessionStorage);
    }
  }
}
