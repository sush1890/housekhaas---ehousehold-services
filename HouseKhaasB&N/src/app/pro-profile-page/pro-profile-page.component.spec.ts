import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProProfilePageComponent } from './pro-profile-page.component';

describe('ProProfilePageComponent', () => {
  let component: ProProfilePageComponent;
  let fixture: ComponentFixture<ProProfilePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProProfilePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProProfilePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
