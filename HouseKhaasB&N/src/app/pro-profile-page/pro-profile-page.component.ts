import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProService } from '../pro.service';
import { OrderService } from '../order.service';

@Component({
  selector: 'app-pro-profile-page',
  templateUrl: './pro-profile-page.component.html',
  styleUrls: ['./pro-profile-page.component.css']
})
export class ProProfilePageComponent implements OnInit {

  orderId=undefined;
  name;
  email;
  mobileNo;
  address;
  zip;
  locality;
  city;
  state;
  skill;
  proId;
  orders = [];

  constructor(private router : Router, private service : ProService, private oservice : OrderService) {
    this.proId = sessionStorage['uId'];
    console.log(this.proId);
    this.getDetails(this.proId);
    this.acceptedOrders(this.proId);
    console.log(this.orderId);
  }

  getDetails(id){
    this.service
      .getDetails(id)
      .subscribe(response => {
        const body = response.json();
        console.log(body)
        this.name=body['name'];
        this.email=body['email'];
        this.mobileNo=body['mobileNo'];
        this.address=body['address'];
        this.zip=body['zip'];
        this.locality=body['locality'];
        this.city=body['city'];
        this.state=body['state'];
        this.skill=body['skill'];
      })
  }

  orderPool() {
    this.router.navigate(['/order-pool'], {queryParams:{"skill":this.skill}});
  }

  acceptedOrders(proId) {
    this.orderCompleted = undefined;
    const temp = parseInt(proId);
    this.service
      .getAcceptedOrders(temp)
      .subscribe(response=>{
        const body = response.json();
        console.log(body);
        this.orders = body;
      })
  }

  orderCompleted() {
    this.oservice
    .orderCompleted(this.orderId)
    .subscribe(response=>{
      this.acceptedOrders(this.proId);
      alert("Order has been marked as completed");  
    })
  }

  completed()
{
  this.oservice
  .orderCompleted(this.orderId)
  .subscribe(response=>{
    this.acceptedOrders(this.proId);
    alert("Order has been marked as completed");  
  })
}
  updateProfessional(id){
    console.log(id);
    this.router.navigate(['/update-pro'], {queryParams: { id: id }});
  }

  ngOnInit() {
  }

}
