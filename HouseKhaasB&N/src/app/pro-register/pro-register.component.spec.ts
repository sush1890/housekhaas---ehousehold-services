import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProRegisterComponent } from './pro-register.component';

describe('ProRegisterComponent', () => {
  let component: ProRegisterComponent;
  let fixture: ComponentFixture<ProRegisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProRegisterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProRegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
