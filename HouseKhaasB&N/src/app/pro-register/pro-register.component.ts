import { Component, OnInit } from '@angular/core';
import { ProService } from '../pro.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pro-register',
  templateUrl: './pro-register.component.html',
  styleUrls: ['./pro-register.component.css']
})
export class ProRegisterComponent implements OnInit {

  name = '';
  password = '';
  email = '';
  mobileNo = '';
  address = '';
  zip = '';
  locality = '';
  city = '';
  state = '';
  skills = [];
  document: any;
  skill = '';

  constructor(private service : ProService, private router: Router) {
    this.selectSkill();
   }

  selectSkill(){
    this.service.getSkill()
    .subscribe(response=>{
      const data = response.json();
      // console.log(data);
      let index = 0;
      data.forEach(element => {
          this.skills[index] = element.skill;
          index = index + 1;
      });
      console.log(this.skills);
    })
  }

  onProSignup(){
    this.service.post(this.document,this.name,this.password,this.email,this.mobileNo,this.address,this.zip,this.locality,this.city,this.state, this.skill)
    .subscribe(response=>{
      console.log(response);
      this.router.navigate(['/']);
    })
  }

  ngOnInit() {
  }

  onZipInsert(zip:any){
    if(zip.target.value.length == 6)
      {
        console.log(zip.target.value);
        this.service.get(zip.target.value)
        .subscribe(response => {
          const body = response.json();
          console.log(body); 
          this.locality = body["locality"];
          this.city = body["city"];
          this.state = body["state"];
        });
      }
  }

  skillSelect(event : any){
    this.skill = event.target.value;
  }

  onSelectImage(event) { 
    console.log(event);
    this.document = event.target.files[0];
    console.log(this.document);
  }
}
