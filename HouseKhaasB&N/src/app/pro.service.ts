import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class ProService {

  constructor(private http : Http) { }

  get(zip:any)
  {
    return this.http.get('http://localhost:8080/spring_mvc_hibernate_template/zipCode/zip/' + zip);
  }

  post(document: any, name, password, email, mobileNo, address, zip, locality, city, state, skill) {
    
    const formData = new FormData();
    formData.append('name', name);
    formData.append('password', password);
    formData.append('email', email);
    formData.append('mobileNo', mobileNo);
    formData.append('address', address);
    formData.append('zip', zip);
    formData.append('locality', locality);
    formData.append('city', city);
    formData.append('state', state);
    formData.append('skill', skill);
    formData.append('document', document);

    return this.http.post('http://localhost:8080/spring_mvc_hibernate_template/professional/register', formData);
  }

  getSkill(){
    return this.http.get('http://localhost:8080/spring_mvc_hibernate_template/professional/skill');
  }

  login(email: string, password: string){
    const body = {
      email : email,
      password : password
    }

    const headers = new Headers({'Content-Type': 'application/json'});
    const requestOptions = new RequestOptions({headers: headers});

    return this.http.post('http://localhost:8080/spring_mvc_hibernate_template/professional/login', body, requestOptions);
  }

  getDetails(id: any) {
    return this.http.get('http://localhost:8080/spring_mvc_hibernate_template/professional/getDetails' + '/' + id);
  }

  getAcceptedOrders(id: any) {
    const body = {
      pid:id
    }
    console.log(body);
    const headers = new Headers({'Content-Type' : 'application/json'});
    const requestOptions = new RequestOptions({headers:headers})
    return this.http.post('http://localhost:8080/spring_mvc_hibernate_template/orders/acceptedOrders',body,requestOptions);
  }

  updateProfessional(address, city, document, email, locality, mobileNo, name, password, pid, skill, state, verificationStatus, zip) {
    const body =
    {
      address:address,
      city:city,
      document:document,
      email:email,
      locality:locality,
      mobileNo:mobileNo,
      name:name,
      password:password,
      pid:pid,
      skill:skill,
      state:state,
      verificationStatus:verificationStatus,
      zip:zip
    }
  console.log(body);
  const headers = new Headers({'Content-Type' : 'application/json'});
  const requestOptions = new RequestOptions({headers:headers});
  return this.http.put('http://localhost:8080/spring_mvc_hibernate_template/professional/update', body, requestOptions);
  }

}
