import { Component, OnInit } from '@angular/core';
import { ProService } from '../pro.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-update-pro',
  templateUrl: './update-pro.component.html',
  styleUrls: ['./update-pro.component.css']
})
export class UpdateProComponent implements OnInit {

  proId:number;

  name:String;
  mobileNo:String;
  email:String;
  password:String;
  professional;

  constructor(private router : Router, private service : ProService, private activeRoute : ActivatedRoute) { 
    activeRoute
    .queryParams
    .subscribe((params)=> {
      this.proId=params.id;
      console.log("In Update : "+this.proId);
      service
      .getDetails(this.proId)
      .subscribe(response => {
        const body = response.json();
        console.log(body);
        this.name=body.name;
        this.email=body.email;
        this.mobileNo=body.mobileNo;
        this.password=body.password;
      })
    })
  }

  // pid: 1, name: "Kailas", email: "kailas@gmail.com", password: "1234", mobileNo: "9999999999", …}
  // address: "Pune"
  // city: "Villa"
  // document: null
  // email: "kailas@gmail.com"
  // locality: "Bibwewadi"
  // mobileNo: "9999999999"
  // name: "Kailas"
  // password: "1234"
  // pid: 1
  // skill: "painting"
  // state: "MH"
  // verificationStatus: true
  // zip: "411052"

  updateProfessional() {
    this.professional=JSON.parse(sessionStorage['user']);
    this.professional.name=this.name;
    this.professional.mobileNo=this.mobileNo;
    this.professional.email=this.email;
    this.professional.password=this.password;
    console.log(this.professional);
    this.service
    .updateProfessional(this.professional.address, this.professional.city, this.professional.document, this.professional.email, this.professional.locality, this.professional.mobileNo, this.professional.name, this.professional.password, this.professional.pid, this.professional.skill, this.professional.state, this.professional.verificationStatus, this.professional.zip)
    .subscribe(response=>{
      if(response.status==200){
        alert("Your Details have been updated successfully");
        this.router.navigate(["/pprofile-page"]);
      }
      else{
        alert("Something Went Wrong...");
      }
    })
  }

  ngOnInit() {
  }

}
