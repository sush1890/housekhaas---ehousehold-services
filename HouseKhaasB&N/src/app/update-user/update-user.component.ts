import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.css']
})
export class UpdateUserComponent implements OnInit {

  userId:number;

  name:String;
  mobileNo:String;
  email:String;
  password:String;
  user;

  constructor(private router : Router, private service : UserService, private activeRoute : ActivatedRoute) { 
    activeRoute
    .queryParams
    .subscribe((params)=> {
      this.userId=params.id;
      service
      .getDetails(this.userId)
      .subscribe(response => {
        const body = response.json();
        console.log(body);
        this.name=body.name;
        this.email=body.email;
        this.mobileNo=body.mobileNo;
        this.password=body.password;
      })
    })
  }

  updateUser() {
    this.user=JSON.parse(sessionStorage['user']);
    this.user.name=this.name;
    this.user.mobileNo=this.mobileNo;
    this.user.email=this.email;
    this.user.password=this.password;
    this.service
    .updateUser(this.user.address, this.user.cid, this.user.city, this.user.email, this.user.locality, this.user.mobileNo, this.user.name, this.user.password, this.user.state, this.user.zip)
    .subscribe(response=>{
      console.log(response.status);
      if(response.status==200){
        alert("Your Details have been updated successfully");
        this.router.navigate(["/uprofile-page"]);
      }
      else{
        alert("Something Went Wrong...");
      }
    })
  }

  ngOnInit() {
  }

}
