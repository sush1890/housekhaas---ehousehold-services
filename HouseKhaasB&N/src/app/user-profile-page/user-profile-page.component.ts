import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-profile-page',
  templateUrl: './user-profile-page.component.html',
  styleUrls: ['./user-profile-page.component.css']
})
export class UserProfilePageComponent implements OnInit {

  name;
  email;
  mobileNo;
  address;
  zip;
  locality;
  city;
  state;
  userId:number;
  orders=[];

  //order: {};
  user: {};

  constructor(private router : Router ,private service : UserService) {
    this.userId = sessionStorage['uId'];
    this.getDetails(this.userId);
    this.loadOrders(this.userId);
  }

  getDetails(id){
    console.log("inside get file");
    this.service
      .getDetails(id)
      .subscribe(response => {
        const body = response.json();
        console.log(body)
        this.name=body['name'];
        this.email=body['email'];
        this.mobileNo=body['mobileNo'];
        this.address=body['address'];
        this.zip=body['zip'];
        this.locality=body['locality'];
        this.city=body['city'];
        this.state=body['state'];
        this.user=body;
        console.log(this.user);
        sessionStorage['user']=JSON.stringify(this.user);
        console.log(sessionStorage['user']);
      })
  }

  loadOrders(id) {
    this.service
      .getAllOrders(id)
      .subscribe(response=>{
        const body = response.json();
        console.log(body);
        this.orders = body;
      })
  }

  updateUser(id){
    console.log(id);
    this.router.navigate(['/update-user'], {queryParams: { id: id }});
  }

  cancelOrder(oid) {
    this.service
    .cancelOrder(oid)
    .subscribe(response => {
      alert("Order was cancelled succesfully");
      this.loadOrders(this.userId);
    })
  }

  ngOnInit() {
  }

}
