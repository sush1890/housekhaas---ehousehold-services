import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-user-register',
  templateUrl: './user-register.component.html',
  styleUrls: ['./user-register.component.css']
})
export class UserRegisterComponent implements OnInit {

  name:String;
  password:String;
  email:String;
  mobileNo:String;
  address:String;
  zip:String;
  locality:String;
  city:String;
  state:String;

  constructor(private router : Router, private service : UserService) { }

  onZipInsert(zip:any){
    if(zip.target.value.length == 6){
      console.log(zip.target.value);
      this.service.get(zip.target.value)
      .subscribe(response => {
        const body = response.json();
        console.log(body); 
        this.locality = body["locality"];
        this.city = body["city"];
        this.state = body["state"];
      });
    }
  }

  onSignup(){
    this.service
    .post(this.name,this.password,this.email,this.mobileNo,this.address,this.zip,this.locality,this.city,this.state)
    .subscribe(response=>{
      console.log(response);
    })
    alert("You have been registered successfully");
    this.router.navigate(['/']);
  }

  ngOnInit() {
  }

}
