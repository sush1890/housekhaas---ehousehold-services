import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UserService implements CanActivate {

  constructor(private http : Http, private router : Router) { }

  canActivate(route:ActivatedRouteSnapshot, state:RouterStateSnapshot){
    if(sessionStorage['login_status'] == '1'){
      return true;
    }
    alert("Please Login");
    this.router.navigate(['/']);
  }

  get(zip : any)
  {
    return this.http.get('http://localhost:8080/spring_mvc_hibernate_template/zipCode/zip/' + zip);
  }

  post(name,password,email,mobileNo,address,zip,locality,city,state){
    const body = {
      address:address,
      email:email,
      mobileNo:mobileNo,
      name:name,
      password:password,     
      zip:zip,
      city:city,
      locality:locality,
      state:state
    }
    const headers = new Headers({'Content-Type' : 'application/json'});
    const requestOptions = new RequestOptions({headers:headers})

    return this.http.post('http://localhost:8080/spring_mvc_hibernate_template/customer/register', body, requestOptions);
  }

  login(email: string, password: string){
    const body = {
      email : email,
      password : password
    }

    const headers = new Headers({'Content-Type': 'application/json'});
    const requestOptions = new RequestOptions({headers: headers});

    return this.http.post('http://localhost:8080/spring_mvc_hibernate_template/customer/login', body, requestOptions);
  }

  getDetails(id: any) {
    return this.http.get('http://localhost:8080/spring_mvc_hibernate_template/customer/getDetails' + '/' + id);
  }

  getAllOrders(id : any) {
    console.log(id);
    return this.http.get('http://localhost:8080/spring_mvc_hibernate_template/orders/userOrderList/' + id);
  }

  submitFeedback(oid,name,email,message) {
    const body = {
      oid:oid,
      name:name,
      email:email,
      message:message
    }
    console.log(body);
    const headers = new Headers({'Content-Type' : 'application/json'});
    const requestOptions = new RequestOptions({headers:headers})

    return this.http.post('http://localhost:8080/spring_mvc_hibernate_template/customer/feedback', body, requestOptions);
  }

  updateUser(address, cid, city, email, locality, mobileNo, name, password, state, zip) {
    const body =
      {
        address:address,
        cid:cid,
        city:city,
        email:email,
        locality:locality,
        mobileNo:mobileNo,
        name:name,
        password:password,
        state:state,
        zip:zip
      }
    console.log(body);
    const headers = new Headers({'Content-Type' : 'application/json'});
    const requestOptions = new RequestOptions({headers:headers});
    return this.http.put('http://localhost:8080/spring_mvc_hibernate_template/customer/update', body, requestOptions);
  }

  cancelOrder(oid) {
    const body = {
      oid:oid
    }
    console.log(body);
    const headers = new Headers({'Content-Type' : 'application/json'});
    const requestOptions = new RequestOptions({headers:headers});
    return this.http.post('http://localhost:8080/spring_mvc_hibernate_template/orders/cancelOrder', body, requestOptions);
  }
}