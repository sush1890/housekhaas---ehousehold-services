const express = require('express');
const bodyParser = require('body-parser');
const studentRouter = require('./students');

const app = express();

app.use(function(request, response, next) {
    response.header("Access-Control-Allow-Origin", "*");
    response.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
    response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use(studentRouter);

app.listen(3000, '0.0.0.0', () => {
    console.log(`Server started on port 3000`);
});
