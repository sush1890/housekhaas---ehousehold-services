const express = require('express');
const db = require('./db');
const utils = require('./utils');

const router = express.Router();

router.get('/students', (request, response) => {
    const connection = db.connect();
    const statement = `select * from students`;
    connection.query(statement, (error, data) => {
        connection.end();
        response.send(utils.createResponse(error, data));
    });
});

router.post('/students', (request, response) => {
    const roll_no = request.body.roll_no;
    const name = request.body.name;
    const percentage = request.body.percentage;
    const connection = db.connect();
    const statement = `insert into students (roll_no, name, percentage) values(${roll_no}, '${name}', ${percentage})`;
    connection.query(statement, (error, data) => {
        connection.end();
        response.send(utils.createResponse(error, data));
    });
});

router.put('/students/:id', (request, response) => {
    const id = request.params.id;
    const roll_no = request.body.roll_no;
    const name = request.body.name;
    const percentage = request.body.percentage;
    const connection = db.connect();
    const statement = `update students set roll_no=${roll_no}, name='${name}', percentage=${percentage} where id=${id}`;
    connection.query(statement, (error, data) => {
        connection.end();
        response.send(utils.createResponse(error, data));
    });
});

router.delete('/students/:id', (request, response) => {
    const id = request.params.id;
    const connection = db.connect();
    const statement = `delete from students where id = ${id}`;
    connection.query(statement, (error, data) => {
        connection.end();
        response.send(utils.createResponse(error, data));
    });
});

module.exports = router;